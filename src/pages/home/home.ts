import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { ModuleProvider } from '../../providers/module/module';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

	modules: Object[] = [];

	constructor(public navCtrl: NavController, public authProvider: AuthProvider, public moduleProvider: ModuleProvider) {

	}

	ionViewDidLoad() {

		this.modules = this.moduleProvider.getModules();

	}

	openModule(module){

		this.navCtrl.push('LessonSelectPage', {
			module: module
		});

	}

	logout(){
		this.authProvider.logout();
	}
	
}