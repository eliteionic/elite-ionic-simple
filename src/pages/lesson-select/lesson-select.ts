import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-lesson-select',
  templateUrl: 'lesson-select.html',
})
export class LessonSelectPage {

	lessons: Object[] = [];

	constructor(public navCtrl: NavController, public navParams: NavParams) {

	}

	ionViewDidLoad(){
		
		let module = this.navParams.get('module');

		if(typeof(module) != 'undefined'){
			this.lessons = module.lessons;
		} else {
			this.navCtrl.setRoot('HomePage');
		}
		
	}

	openLesson(lesson){
		this.navCtrl.push('LessonPage', {lesson: lesson});
	}

}