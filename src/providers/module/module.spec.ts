import { ModuleProvider } from './module';
 
let moduleProvider;
 
describe('Provider: Module', () => {
 
    beforeEach(() => {
      moduleProvider = new ModuleProvider();
    });
 
    it('the getModules function should return an array', () => {

		expect(moduleProvider.getModules() instanceof Array).toBe(true);
 
    });
 
});