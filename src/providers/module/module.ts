import { Injectable } from '@angular/core';

@Injectable()
export class ModuleProvider {

	constructor() {
		console.log('Hello ModuleProvider Provider');
	}

	getModules(){

		let modules = [
			{	
				title: 'Module One', 
				description: 'Test', 
				lessons: [
					{title: 'lesson 1', content: 'this is the lesson content'},
					{title: 'lesson 2', content: 'this is the lesson content'}
				]
			},
			{	
				title: 'Module Two', 
				description: 'Test', 
				lessons: [
					{title: 'lesson 1', content: 'this is the lesson content'},
					{title: 'lesson 2', content: 'this is the lesson content'}
				]
			},
			{	
				title: 'Module Three', 
				description: 'Test', 
				lessons: [
					{title: 'lesson 1', content: 'this is the lesson content'},
					{title: 'lesson 2', content: 'this is the lesson content'}
				]
			},
			{	
				title: 'Module Four', 
				description: 'Test', 
				lessons: [
					{title: 'lesson 1', content: 'this is the lesson content'},
					{title: 'lesson 2', content: 'this is the lesson content'}
				]
			},
			{	
				title: 'Module Five', 
				description: 'Test', 
				lessons: [
					{title: 'lesson 1', content: 'this is the lesson content'},
					{title: 'lesson 2', content: 'this is the lesson content'}
				]
			},
		];

		return modules;

	}

}