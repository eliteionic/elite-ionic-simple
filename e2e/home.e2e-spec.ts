import { browser, protractor } from 'protractor';
import { HomePageObject } from './page-objects/home.page-object';
import { LoginPageObject } from './page-objects/login.page-object';
import { LessonSelectPageObject } from './page-objects/lesson-select.page-object';

describe('Home', () => {

	let homePage: HomePageObject;
	let loginPage: LoginPageObject;
	let lessonSelectPage: LessonSelectPageObject;

	beforeEach(() => {
		
		homePage = new HomePageObject();
		loginPage = new LoginPageObject();
		lessonSelectPage = new LessonSelectPageObject();
		homePage.browseToPage();

	});

	it('should be able to view a list of modules', () => {

		expect<any>(homePage.getModuleListItems().count()).toBe(5);

	});

	it('the list of modules should contain the titles of the modules', () => {
	
		expect<any>(homePage.getModuleListItems().first().getText()).toContain('Module One');

	});

	it('after selecting a specific module, the user should be able to see a list of available lessons', () => {
	
		let moduleToTest = homePage.getModuleListItems().first();

		moduleToTest.click();

		expect<any>(lessonSelectPage.getLessonListItems().count()).toBeGreaterThan(1);

	});

	it('should be able to log out and back in', () => {

		homePage.getLogoutButton().click();

		browser.wait(protractor.ExpectedConditions.not((protractor.ExpectedConditions.urlContains('home'))));

		/* Log back in to prevent rest of tests breaking */

		let input = loginPage.getKeyInput();
		let loginButton = loginPage.getLoginButton();

		input.sendKeys('abcd-egfh-ijkl-mnop');

		loginButton.click();

		browser.wait(protractor.ExpectedConditions.urlContains('home'));		

	});

});