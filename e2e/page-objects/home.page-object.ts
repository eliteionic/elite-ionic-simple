import { browser, protractor, element, by, ElementFinder } from 'protractor';
import { AppPageObject } from './app.page-object';

export class HomePageObject {

  appPage = new AppPageObject();

  browseToPage(){

    browser.get('');

    browser.wait(protractor.ExpectedConditions.urlContains('home'));

    this.appPage.waitForClickBlock();

  }

  getModuleListItems(){
    return element.all(by.css('.module-list button'));
  }

  getLogoutButton(){
    return element(by.css('.logout-button'));
  }

}