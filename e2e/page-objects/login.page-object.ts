import { browser, protractor, element, by, ElementFinder } from 'protractor';
import { HomePageObject } from './home.page-object';

export class LoginPageObject {

	homePage = new HomePageObject();

	browseToPage(){
		
		this.homePage.browseToPage();
		this.homePage.getLogoutButton().click();
		browser.wait(protractor.ExpectedConditions.not((protractor.ExpectedConditions.urlContains('home'))));
		
	}

	getKeyInput() {
		return element.all(by.css('.key-input input'));
	}

	getLoginButton() {
		return element.all(by.css('.login-button'));
	}

}